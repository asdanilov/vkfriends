package org.asdanilov.work.vkfriends;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilites singleton class
 * Created by Alex on 29.08.2014.
 */
public class Utilites {

    private static volatile Utilites instance;

    private static final String TAG = "Utilites";
    public static final String API_VERSION="5.5";
    public static String redirect_url = "https://oauth.vk.com/blank.html";
    public static String BASE_URL = "https://api.vk.com/method/friends.get?user_id=";
    public static String FIELDS = "&fields=first_name,last_name,photo_200_orig";

    private Utilites(){}

    public static Utilites getInstance() {
        if (instance == null) {
            synchronized(Utilites.class) {
                if (instance == null) {
                    instance = new Utilites();
                }
            }
        }
        return instance;
    }

    public String getUrl(String api_id, String settings){
        String url="https://oauth.vk.com/authorize?client_id="+api_id+"&display=mobile&scope="+settings+"&redirect_uri="+ URLEncoder.encode(redirect_url)+"&response_type=token"
                +"&v="+URLEncoder.encode(API_VERSION);
        return url;
    }

    public String getSettings(){
        return "friends,photos";
    }

    public String[] parseRedirectUrl(String url) {

        String access_token = extractPattern(url, "access_token=(.*?)&");
        String user_id = extractPattern(url, "user_id=(\\d*)");

        return new String[]{access_token, user_id};
    }

    public String extractPattern(String string, String pattern){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        if (!m.find())
            return null;
        return m.toMatchResult().group(1);
    }

    public String unescape(String text){
        if(text==null)
            return null;
        return text.replace("&amp;", "&").replace("&quot;", "\"").replace("<br>", "\n").replace("&gt;", ">").replace("&lt;", "<")
                .replace("<br/>", "\n").replace("&ndash;","-").trim();
    }


    public String convertStreamToString(InputStream is) throws IOException {
        InputStreamReader r = new InputStreamReader(is);
        StringWriter sw = new StringWriter();
        char[] buffer = new char[1024];
        try {
            for (int n; (n = r.read(buffer)) != -1;)
                sw.write(buffer, 0, n);
        }
        finally{
            try {
                is.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return sw.toString();
    }
}
