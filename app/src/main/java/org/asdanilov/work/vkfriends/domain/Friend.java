package org.asdanilov.work.vkfriends.domain;

import org.asdanilov.work.vkfriends.Utilites;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by work on 29.08.2014.
 */
public class Friend {
    private String photo;
    private String first_name;
    private String last_name;


    public static Friend parse(JSONObject o) throws JSONException {
        Friend u = new Friend();

        if(!o.isNull("first_name"))
            u.first_name = Utilites.getInstance().unescape(o.getString("first_name"));
        if(!o.isNull("last_name"))
            u.last_name = Utilites.getInstance().unescape(o.getString("last_name"));
        if(!o.isNull("photo_200_orig"))
            u.photo = o.optString("photo_200_orig");
        return u;
    }

    public String getPhoto() {
        return photo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }
}
