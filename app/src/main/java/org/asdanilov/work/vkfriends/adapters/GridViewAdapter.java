package org.asdanilov.work.vkfriends.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.asdanilov.work.vkfriends.R;
import org.asdanilov.work.vkfriends.domain.Friend;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Grid view adapter
 * Created by Alex on 29.08.2014.
 */
public class GridViewAdapter extends BaseAdapter {

    private static final String TAG = GridViewAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<Friend> friends;
    private int resource;

    private static LruCache<String, Bitmap> memoryCache = null;
    private static int cacheSize;

    public GridViewAdapter(Context context, int resource, ArrayList<Friend> friends) {

        this.context = context;
        this.friends = friends;
        this.resource = resource;

//        final int memClass = ((ActivityManager) context.getSystemService(
//                Context.ACTIVITY_SERVICE)).getMemoryClass();
//        cacheSize = 1024 * 1024 * memClass / 8;

        cacheSize = friends.size() * 200 * 1024;
    }

    @Override
    public int getCount() {
        return friends.size();
    }

    @Override
    public Object getItem(int i) {
        return friends.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View row = view;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(resource, null);

        } else {
            row = view;
        }

        TextView textView = (TextView) row.findViewById(R.id.grid_item_label);
        textView.setText(friends.get(i).getLast_name() + " " + friends.get(i).getFirst_name());

        ImageView imageView = (ImageView) row.findViewById(R.id.image_view);

        loadToView(friends.get(i).getPhoto(), imageView, i);

        return row;
    }

    public void loadToView(String url, ImageView view, int i) {
        if (url == null || url.length() == 0) return;
        if (memoryCache == null) {
            memoryCache = new LruCache<String, Bitmap>(cacheSize) {
                @Override
                protected int sizeOf(String key, Bitmap bitmap) {
                    return (bitmap.getRowBytes() * bitmap.getHeight());
                }
            };
        }

        Bitmap bitmap = getBitmapFromMemCache(url);
        if (bitmap == null) {
            final CachedImageAsyncTask task = new CachedImageAsyncTask(view, i, context);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
        } else {
            view.setImageBitmap(bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return memoryCache.get(key);
    }

    /**
     * AsyncTaskClass for loading photos
     */
    public static class CachedImageAsyncTask extends AsyncTask<String, Void, Bitmap> {

        private static final String TAG = CachedImageAsyncTask.class.getSimpleName();
        private final Integer position;

        private ImageView imageView;
        private Context context;

        private AlertDialog alertDialog;

        public CachedImageAsyncTask(ImageView img, int position, Context context) {
            this.imageView = img;
            imageView.setTag(position);
            imageView.setImageBitmap(null);
            this.position = position;
            this.context = context;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {

            String url = strings[0];

            Bitmap result = null;

            if (url != null) {
                try {

                    result = load(url);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (result != null) {
                    memoryCache.put(url, result);
                }
            }

            return result;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {

            if (imageView.getTag() == this.position) {
                imageView.setTag(null);
                if ((bitmap != null)) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }

        public static Bitmap load(String urlString) throws IOException {
            if (urlString == null || urlString.length() == 0) return null;

            Bitmap bitmap;
            URL url;

            url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();

            return bitmap;
        }
    }
}
