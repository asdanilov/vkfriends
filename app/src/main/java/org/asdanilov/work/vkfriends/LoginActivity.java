package org.asdanilov.work.vkfriends;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Login activity main class
 * Created by Alex on 29.08.2014.
 */
public class LoginActivity extends Activity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //check internet connection
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();

        if (nInfo != null && nInfo.isConnectedOrConnecting()) {

            WebView webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.clearCache(true);
            webView.setWebViewClient(new VkWebViewClient());
            webView.loadUrl(Utilites.getInstance().getUrl(getResources().getString(R.string.vk_app_id), Utilites.getInstance().getSettings()));
        } else {

            new AlertDialog.Builder(this)
                    .setTitle("Connection")
                    .setMessage("No internet connection!")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

    }

    /**
     * Class for modifying webView behaviour
     */
    class VkWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            if(url == null)
                return;

            if(url.startsWith(Utilites.redirect_url))
            {
                if(!url.contains("error=")){
                    String[] auth = Utilites.getInstance().parseRedirectUrl(url);

                    Log.i(TAG, auth[0] + " : " + auth[1]);

                    Intent friendListActivityIntent = new Intent(getApplicationContext(), FriendListActivity.class);
                    friendListActivityIntent.putExtra("user_id", auth[1]);
                    friendListActivityIntent.putExtra("token", auth[0]);
                    startActivity(friendListActivityIntent);
                }
                finish();
            }
        }
    }
}