package org.asdanilov.work.vkfriends;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.GridView;

import org.asdanilov.work.vkfriends.adapters.GridViewAdapter;
import org.asdanilov.work.vkfriends.domain.Friend;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Class for friends list display
 * Created by Alex on 29.08.2014.
 */
public class FriendListActivity extends Activity {

    private static final String TAG = FriendListActivity.class.getSimpleName();
    private String user_id;
    private ArrayList<Friend> friends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        final GridView gridView = (GridView) findViewById(R.id.grid_view);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        user_id = getIntent().getExtras().getString("user_id");

        final ProgressDialog dialog = ProgressDialog.show(this, "Obtaining friends", "Please wait...", true);

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    friends = getFriends();
                } catch (IOException e) {
                    showAlertDialog("Transfer", "Connection error, try later");
                } catch (JSONException e) {
                    showAlertDialog("Transfer", "Data error");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                dialog.dismiss();

                GridViewAdapter gridViewAdapter = new GridViewAdapter(getApplicationContext(), R.layout.activity_friend_list_line, friends);
                gridView.setAdapter(gridViewAdapter);
            }
        }.execute();
    }

    private void showAlertDialog(final String title, final String message) {

        runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(getApplicationContext())
                        .setTitle(title)
                        .setMessage(message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

            }
        });
    }

    public ArrayList<Friend> getFriends() throws IOException, JSONException {

        JSONObject root = sendRequest();

        ArrayList<Friend> friends = new ArrayList<Friend>();
        JSONArray array = root.optJSONArray("response");

        if (array == null)
            return friends;

        for (int i = 0; i < array.length(); ++i) {
            JSONObject o = (JSONObject) array.get(i);
            Friend u = Friend.parse(o);
            friends.add(u);
        }
        return friends;
    }

    /**
     * get frinds (json)
     * @return
     */
    private JSONObject sendRequest() throws IOException, JSONException {

        String url = Utilites.BASE_URL + user_id + Utilites.FIELDS;

        HttpURLConnection connection = null;
        JSONObject root = null;

        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setConnectTimeout(30000);
            connection.setReadTimeout(30000);
            connection.setUseCaches(false);
            connection.setDoOutput(false);
            connection.setDoInput(true);
            connection.setRequestMethod("GET");

            int code = connection.getResponseCode();
            Log.i(TAG, "code=" + code);

            InputStream is = new BufferedInputStream(connection.getInputStream(), 8192);
            String response = Utilites.getInstance().convertStreamToString(is);
            root = new JSONObject(response);

        } finally {
            if (connection != null)
                connection.disconnect();
        }

        return root;
    }

}
